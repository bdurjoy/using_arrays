/*********************************************************************************************
 * 
 * Student Name: Durjoy Barua
 * Student Number: 040919612
 * Course:  19W CST8130 - Data Structures
 * 
 * 
 */

import java.util.*;
public class Lab1Main {

	public static void main (String [] args) {

		// write the code here to implement the menu as specified in Lab 1

		Lab1Main user = new Lab1Main();

		user.userInteraction();

	}
	
	
	
	public void userInteraction() {
		Numbers numbers = new Numbers();
		Scanner input = new Scanner(System.in);
		int userInput = 0; //just a variable to run through the while loop
		boolean program_run = true;//boolean var to keep the program running
		boolean sorted = false; //boolean variable to keep the track of number if it's sorted

		do{
			System.out.print("Enter 1 to initialize a default array,\n"
					+ "2 to initialize an array of input size,\n"
					+ "3 to fill array with values,\n"
					+ "4 to display values in array,\n"
					+ "5 to display average of the values in the array,\n"
					+ "6 to take the file input, \n"
					+ "7 to sort the numbers of array, \n"
					+ "8 to search number in different range in the array, \n"
					+ "9 to exit the program\n");
			
			try {
				userInput = input.nextInt();
			}catch(InputMismatchException e) {
				input.next();
				userInput = 0;
				
			}
			
			
			if(userInput == 1) {
				//It is initialized as default 10
				numbers = new Numbers();

			}else if(userInput == 2) {
				//user asked to set the size of the array
				int size = 0;
				numbers = new Numbers(size);
			}else if(userInput == 3) {

				//this section asks user to input values into the array
				numbers.initValuesInArray();
			
			}else if(userInput == 4) {

				//display values in array
				System.out.println("Values are: ");
				System.out.print(numbers);

			}else if(userInput == 5) {
				//It helps user to calculate the average of the values of the array
				System.out.println(numbers.calcAverage());
			}else if(userInput == 6) {
				numbers.fileRead();
				sorted = false;
			}else if(userInput == 7){
				//It helps the user to sort the array of numbers
				numbers.sortArray();
				sorted = true;
				
			}else if(userInput == 8) {
				//This helps user to search in the array
				if(sorted == true) {
					numbers.searchInRange();
				}else {
					System.out.println("Cannot search before sorting!");
				}
				
			}
			else {
				//If user input wrong value it takes user out.
				System.out.println("Invalid entry..");
			}
		}while(program_run != false);

	}
}