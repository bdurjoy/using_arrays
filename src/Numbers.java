/*********************************************************************************************
 * 
 * Student Name: Durjoy Barua
 * Student Number: 040919612 
 * Course:  19W CST8130 - Data Structures
 * 
 * This class contains the dynamically allocated array and it's processing
 */
import java.io.*;
import java.util.*;
public class Numbers {
	private Float [] numbers;
	private int size;
	Scanner input = new Scanner(System.in);

	public Numbers() {
		///  write code here to initialize a "default" array since this is the default constructor
		//this default constructor initialize value of an array of 10 size
		numbers = new Float[10];

	}

	public Numbers (int size) {
		///  write code here to initialize an "initial" array given the parameter size as this is an initial constructor
		// catch negative size as well as non-numerical input for array
		this.size = size;
		System.out.println("Enter the size of the array: ");

		try{	//this try method checks the user input as if it is integer, and throw an exception for invalid input
			this.size = input.nextInt();

			if(this.size>0) { //this statement checks if the size user inputs is positive
				numbers = new Float[this.size];	
			}else {
				throw new InputMismatchException();
			}
		}catch(InputMismatchException e) {
			System.out.println("Sorry! Invalid Entry");
			numbers = new Float[10]; 
		}
	}


	public void initValuesInArray() {
		/// write code here to initialize the values in the array

		System.out.println("Enter the float numbers as values in the array:\n");
		for(int i = 0; i < this.size; i++) {
			System.out.print("Enter Values: ");

			try {
				numbers[i] = input.nextFloat();
			}catch(InputMismatchException e) {
				input.next();
				i--;

				System.out.println("Invalid Entry...\nEnter Valid number!!!");
			}
		}

		System.out.println();
	}

	public String toString() {
		// write code to return the values in the array in a list format - one value per line
		String out = "";

		for(Float number: numbers) {
			if(number == null) {
				number = 0.0f;
			}
			out +=number+"\n";
		}

		return out;
	}

	public float calcAverage() {
		// write code to return the average of the values in the array
		float sum = 0.0f;
		if(numbers[0] == null) {
			return 0.0f;
		}

		for(Float num: numbers) {
			sum += num;
		}
		float avg = sum/numbers.length;

		return avg;
	}



	public void fileRead() {
		//Asks the user to input the file name and it directs the values to the method

		Scanner fileData= null;
		System.out.print("Enter the file name: ");
		String fileName = input.next();
		File textFile = new File(fileName);
		numbers = new Float[10];


		try {

			fileData = new Scanner(textFile);
			boolean intCheck = fileData.hasNextInt();
			Float[] temp = null;

			if(intCheck) {
				this.size = fileData.nextInt();
				temp = new Float[size];
			}else {
				throw new InputMismatchException();
			}

			for(int i = 0; i<size; i++) {
				temp[i] = fileData.nextFloat();
			}


			numbers = new Float[size];
			numbers = temp;


		}catch(FileNotFoundException nE) {
			System.out.println(fileName+" file not found");
		}catch(InputMismatchException fD) {
			System.out.println("File Corrupted!");
		}catch(NoSuchElementException nS) {
			System.out.println("Missing entries..");
		}
	}


	public void sortArray() {
		System.out.println("Array is sorted as per user instruction: ");
		Float temp;
		int i, j;
		for(i = 0; i < size; i++ ){
			j = i;
			while(j > 0 && numbers[j] < numbers[j-1]) {
				temp = numbers[j-1];
				numbers[j-1] = numbers[j];
				numbers[j] = temp;
				j--;
			}
		}
		System.out.println(toString());
	}

	public void searchInRange() {
		Float low, high; //Declaring two variable to search in strings
		int count = 0; //Variable that adds up late to count the numbers in range
		boolean end = false;
		while(end == false) {
			try {
				System.out.println("Enter Low Value: ");
				low = input.nextFloat();
				System.out.println("Enter High Value: ");
				high = input.nextFloat();


				if(high < low) {
					System.out.println("Error! high must be larger than low. Enter Again");
				}else {
					for(int j = 0; j < size; j++) {
						if(numbers[j] > low && numbers[j] < high) {
							count++;
						}else if(high <= numbers[j]) {
							break;
						}
					}
					end = true;
				}
			}catch(InputMismatchException iM) {
				System.out.println("Range is not valid!");
				input.next();
			}
		}
		System.out.println("Count Display: "+count);
	}

}
